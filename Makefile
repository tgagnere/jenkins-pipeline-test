all : executable
executable : test.o
	gcc -o executable test.o
test.o : test.c
	gcc -c test.c
clean:
	rm test.o executable
